const router = require('express').Router()
const spreeClient = require('../../../services/spree.client')

/* 
 Array containing Id(s) of "special sale(s), e.g. 'Summer Sale' "
*/
const SPECIAL_SALES_IDS = ["22"]

router.get('/', async (req, res, next) => {
  try{
    const categories = await spreeClient.getCategories()

    const categoriesParsedData = categories["data"].map( category => {
      return {id: category.id , ...category.attributes}
    })
  
    const caterogiesWithSales = categoriesParsedData.map(category => {
      category["isSpecial"] = SPECIAL_SALES_IDS.includes(category.id)
      return category 
    })
  
    res.json(caterogiesWithSales)
  } 
  catch (err) {
    next(err);
  }
})

router.get('/:id', async (req, res, next) => {
  try{
    const {data} = await spreeClient.getCategory(req.params.id)
    res.json(data)
  }
  catch(err){
    next(err)
  }
})

module.exports = router