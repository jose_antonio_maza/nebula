const router = require('express').Router()
const spreeClient = require('../../../services/spree.client')

router.get('/', async (req, res, next) => {
  try{
    const raw_products = await spreeClient.getProducts(req.query)

    const parsed_products = raw_products.data.map(product => {
      return {id: product.id , ...product.attributes}
    })
  
    res.json({parsed_products, meta: raw_products.meta })
  }
  catch (err) {
    next(err);
  }
})

router.get('/:id', async (req, res, next) => {
  try{
    const {data} = await spreeClient.getProduct(req.params.id)
    res.json(data)
  }
  catch(err){
    next(err)
  }
})


module.exports = router