const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 8050;

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());


app.use('/api/v1', require('./api/v1/routes'))

app.use((err, _rq, res, _next) => {
  console.log(err)
  const status = err.statusCode || 500;
  const message = err.message
  const data = err.data
  res.status(status).json({message, data})
});

app.listen(port, () => console.log(`Server listening on port ${port}!`))