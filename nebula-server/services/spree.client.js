const axios = require('axios')
const qs = require('qs')

const spreeBaseEndpoint = `http://localhost:3000/api/v2/storefront`

/*
 Returns list of Home categories for Nebula App (Only root categories from spree)
*/
async function getCategories() {
  try {
    const rootCategory = await _getRootCategory();

    const rootCategories = _parseCategoriesIntoIds(rootCategory)
    const res = await axios
      .get(
        `${spreeBaseEndpoint}/taxons?filter[ids]=${rootCategories}`
      );
    return res.data;
  } catch (err) {
    throw err;
  }
}

/*
For listing all products, accepts a queryStr with filters
returns data with objects of products
*/
async function getProducts(queryStr = null) {
  const category = queryStr["categoryId"] ? queryStr.categoryId : null
  const price = queryStr["price"] ? queryStr.price : null
  const page = queryStr["page"] ? queryStr.page : null
  const per_page = queryStr["per_page"] ? queryStr.per_page : null
  
  try {
    const res = await axios
      .get(
        `${spreeBaseEndpoint}/products`, { 
          params: { 
            filter: { taxons: category, price: price },
            page: page,
            per_page: per_page 
          },
          paramsSerializer: (params) => {
            return qs.stringify(params);
          }
        });
    return res.data;
  } catch (err) {
    throw err;
  }
}

/*
Returns Product from Id
*/
async function getProduct(productId) {
  try {
    const res = await axios
      .get(
        `${spreeBaseEndpoint}/products/${productId}`,
      );
    return res.data;
  } catch (err) {
    throw err;
  }
}

/*
Returns Category from Id
*/
async function getCategory(categoryId) {
  try {
    const res = await axios
      .get(
        `${spreeBaseEndpoint}/taxons/${categoryId}`,
      );
    return res.data;
  } catch (err) {
    throw err;
  }
}

/*
Provides Data for categories endpoint Only root Categories. 
*/
async function _getRootCategory() {
  try {
    const res = await axios
      .get(
        `${spreeBaseEndpoint}/taxons/1`,
      );
    return res.data;
  } catch (err) {
    throw err;
  }
}

function _parseCategoriesIntoIds({ data: { relationships: { children } } }) {
  return children.data.map(root => root.id).join(',')
}

module.exports = {
  getCategories,
  getProducts,
  getProduct,
  getCategory
}

///api/v2/storefront/products?filter[price]=1%2C100&filter[taxons]=2&filter[options][tshirt-color]=blue