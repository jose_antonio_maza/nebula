
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: 'Home', path: '', component: () => import('pages/PageHome.vue') },
      { name: 'Products', path: '/products', component: () => import('pages/PageProducts.vue' ) },
      { name: 'ProductDetails', path: '/:id', component: () => import('pages/PageProductDetails.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
