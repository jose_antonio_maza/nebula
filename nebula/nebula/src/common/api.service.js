import axios from "axios"

// axios base url
//axios.defaults.baseURL = `${process.env.API}/api/v1/`
axios.defaults.baseURL = "http://localhost:8050/api/v1/"

const ApiService = {
  get(resource, params) {
    return axios.get(`${resource}`, params).catch(error => {
      throw new Error(`[Nebula] ApiService ${error}`)
    })
  },

  post(resource, params) {
    return axios.post(`${resource}`, params)
  },

  put(resource, params) {
    return axios.put(`${resource}`, params)
  },

  delete(resource) {
    return axios.delete(resource).catch(error => {
      throw new Error(`[Nebula] ApiService ${error}`)
    })
  }
}

export const ProductsService = {
  query(params){
    return ApiService.get("products", params )
  },
  getProduct(id){
    return ApiService.get(`products/${id}`)
  }
}

export const CategoriesService = {
  get(){
    return ApiService.get("categories")
  },
  async getCategoryName(id){
     const {data: {attributes} } = await ApiService.get(`categories/${id}`);
     return attributes.name;
  }
}
