# Nebula (nebula)

Nebula is a Proof-of-Concept application in the eCommerce space - Nebulas are interstellar clouds of particles that give birth to stars

You can run this app using docker or locally.

## Using Docker:
```bash
docker build -t nebula-front .
```

then:

```bash
docker run -p 8080:8080 nebula-front
```

## If running locally: 1. Install the dependencies
```bash
npm install
```
## 2. Run Dev
npm run dev





### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
